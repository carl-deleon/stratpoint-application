package com.stratpoint.stratpointassessment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.stratpoint.stratpointassessment.fragment.MovieDetailFragment;
import com.stratpoint.stratpointassessment.fragment.MovieListFragment;
import com.stratpoint.stratpointassessment.model.Movie;
import com.stratpoint.stratpointassessment.util.AppUtil;
import com.stratpoint.stratpointassessment.util.Constants;
import com.stratpoint.stratpointassessment.util.NetworkAsyncTask;

import java.util.ArrayList;


/**
 * An activity representing a list of Movies. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link MovieDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link MovieListFragment} and the item details
 * (if present) is a {@link MovieDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link MovieListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class MovieListActivity extends AppCompatActivity
        implements MovieListFragment.Callbacks, NetworkAsyncTask.NetworkListener {


    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private ArrayList<Movie> mMovies;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_app_bar);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Downloading Data...");
        mProgressDialog.setCancelable(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        if (findViewById(R.id.movie_detail_container) != null) {
            mTwoPane = true;

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movie_list_container, MovieListFragment.newInstance(true,
                            (mMovies != null) ? mMovies : null), "list")
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.movie_list_container, MovieListFragment.newInstance(false,
                            (mMovies != null) ? mMovies : null), "list")
                    .commit();
        }

        if (savedInstanceState == null) {
            // Download data from web service
            NetworkAsyncTask mAsyncTask = new NetworkAsyncTask(this);
            mAsyncTask.setListener(this);
            mAsyncTask.execute(Constants.BASE_URL);
            mProgressDialog.show();
        }
    }

    /**
     * Callback method from {@link MovieListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String position) {
        int itemPosition = Integer.parseInt(position);

        if (mTwoPane) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.movie_detail_container,
                            MovieDetailFragment.newInstance(mMovies.get(itemPosition)))
                    .commit();

        } else {
            Intent detailIntent = new Intent(this, MovieDetailActivity.class);
            detailIntent.putExtra("detail", mMovies.get(itemPosition));
            startActivity(detailIntent);
        }
    }

    @Override
    public void onSuccess(String response) {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        mMovies = AppUtil.parseMovieJSON(response);

        if (getSupportFragmentManager().findFragmentByTag("list").isAdded()) {
            ((MovieListFragment) getSupportFragmentManager().findFragmentByTag("list"))
                    .setTitles(mMovies);
        }
    }

    @Override
    public void onFailure(String message) {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("list", mMovies);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mMovies = savedInstanceState.getParcelableArrayList("list");

        if (getSupportFragmentManager().findFragmentByTag("list").isAdded()) {
            ((MovieListFragment) getSupportFragmentManager().findFragmentByTag("list"))
                    .setTitles(mMovies);
        }
    }
}
