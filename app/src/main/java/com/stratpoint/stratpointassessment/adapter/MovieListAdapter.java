package com.stratpoint.stratpointassessment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.stratpoint.stratpointassessment.R;
import com.stratpoint.stratpointassessment.model.Movie;
import com.stratpoint.stratpointassessment.util.AppUtil;
import com.stratpoint.stratpointassessment.util.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Carl Justine De leon on 10/23/2015.
 */
public class MovieListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Movie> mMovies;
    private LayoutInflater inflater;
    private ImageLoader mImageLoader;

    public MovieListAdapter(Context context, ArrayList<Movie> movies) {
        mContext = context;
        mMovies = movies;

        mImageLoader = new ImageLoader(mContext);
    }

    @Override
    public int getCount() {
        return (mMovies != null && mMovies.size() > 0) ? mMovies.size() : 0;
    }

    @Override
    public Movie getItem(int position) {
        return mMovies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (inflater == null) {
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_movie, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.yearReleased = (TextView) convertView.findViewById(R.id.year_released);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Movie movie = mMovies.get(position);

        holder.title.setText(movie.getTitle());
        holder.yearReleased.setText(Integer.toString(movie.getYear()));
        mImageLoader.DisplayImage(AppUtil.getMovieCover(movie.getSlug()), holder.thumbnail);

        return convertView;
    }

    public void setTitles(ArrayList<Movie> movies) {
        mMovies = movies;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        public TextView title;
        public TextView yearReleased;
        public ImageView thumbnail;
    }
}
