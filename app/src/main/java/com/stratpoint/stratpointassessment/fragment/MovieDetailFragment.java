package com.stratpoint.stratpointassessment.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stratpoint.stratpointassessment.R;
import com.stratpoint.stratpointassessment.model.Movie;
import com.stratpoint.stratpointassessment.util.AppUtil;
import com.stratpoint.stratpointassessment.util.ImageLoader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MovieDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MovieDetailFragment extends Fragment {

    private Movie mMovie;

    private ImageView mBackdrop, mCover;
    private TextView mTitleLong, mYearReleased, mRating, mOverview;
    private ImageLoader mImageLoader;

    public MovieDetailFragment() {
        // Required empty public constructor
    }

    public static MovieDetailFragment newInstance(Movie movie) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("detail", movie);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMovie = getArguments().getParcelable("detail");
        }

        mImageLoader = new ImageLoader(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        mBackdrop = (ImageView) view.findViewById(R.id.backdrop);
        mCover = (ImageView) view.findViewById(R.id.cover);
        mTitleLong = (TextView) view.findViewById(R.id.title_long);
        mYearReleased = (TextView) view.findViewById(R.id.year_released);
        mRating = (TextView) view.findViewById(R.id.rating);
        mOverview = (TextView) view.findViewById(R.id.overview);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mImageLoader.DisplayImage(AppUtil.getMovieBackdrop(mMovie.getSlug()), mBackdrop);
        mImageLoader.DisplayImage(AppUtil.getMovieCover(mMovie.getSlug()), mCover);
        mTitleLong.setText(mMovie.getTitleLong());
        mYearReleased.setText(Integer.toString(mMovie.getYear()));
        mRating.setText(Float.toString(mMovie.getRating()));
        mOverview.setText(mMovie.getOverview());
    }
}
