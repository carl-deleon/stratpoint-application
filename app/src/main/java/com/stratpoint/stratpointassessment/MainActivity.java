package com.stratpoint.stratpointassessment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.stratpoint.stratpointassessment.model.Movie;
import com.stratpoint.stratpointassessment.util.AppUtil;
import com.stratpoint.stratpointassessment.util.Constants;
import com.stratpoint.stratpointassessment.util.NetworkAsyncTask;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NetworkAsyncTask.NetworkListener {

    private static final String DEBUG_TAG = MainActivity.class.getSimpleName();

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Downloading Data...");
        mProgressDialog.setCancelable(false);

        NetworkAsyncTask mAsyncTask = new NetworkAsyncTask(this);
        mAsyncTask.setListener(this);
        mAsyncTask.execute(Constants.BASE_URL);
        mProgressDialog.show();
    }

    @Override
    public void onSuccess(String response) {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        ArrayList<Movie> movieArrayList = AppUtil.parseMovieJSON(response);

        for (Movie movie : movieArrayList) {
            Log.d(DEBUG_TAG, movie.getTitle());
        }
    }

    @Override
    public void onFailure(String message) {
        Log.e(DEBUG_TAG, message);

        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
