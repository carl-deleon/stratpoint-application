package com.stratpoint.stratpointassessment.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.stratpoint.stratpointassessment.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Carl Justine De leon on 10/23/2015.
 */
public class AppUtil {

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static ArrayList<Movie> parseMovieJSON(String jsonData) {
        ArrayList<Movie> movies = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject jsonObjectData = jsonObject.getJSONObject("data");

            JSONArray jsonArrayMovies = jsonObjectData.getJSONArray(Constants.KEY_ARRAY_MOVIE);

            for (int i = 0; i < jsonArrayMovies.length(); i++) {
                JSONObject jsonObjectMovie = jsonArrayMovies.getJSONObject(i);

                Movie movie = new Movie();
                movie.setYear(jsonObjectMovie.getInt("year"));
                movie.setTitle(jsonObjectMovie.getString("title"));
                movie.setTitleLong(jsonObjectMovie.getString("title_long"));
                movie.setRating(BigDecimal.valueOf(jsonObjectMovie.getDouble("rating")).floatValue());
                movie.setOverview(jsonObjectMovie.getString("overview"));
                movie.setSlug(jsonObjectMovie.getString("slug"));

                movies.add(movie);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return movies;
    }

    public static String getMovieCover(String slug) {
        return Constants.BASE_URL_COVER.replace("{slug}", slug);
    }

    public static String getMovieBackdrop(String slug) {
        return Constants.BASE_URL_BACKDROP.replace("{slug}", slug);
    }
}
