package com.stratpoint.stratpointassessment.util;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Carl Justine De leon on 10/23/2015.
 */
public class NetworkAsyncTask extends AsyncTask<String, Void, Boolean> {

    private Context mContext;
    private NetworkListener mListener;

    private HashMap<String, String> mResponseMap = new HashMap<>();

    public NetworkAsyncTask(Context context) {
        mContext = context;
    }

    /**
     * required in order to prevent issues in earlier Android version.
     */
    private static void disableConnectionReuseIfNecessary() {
        // see HttpURLConnection API doc
        if (Integer.parseInt(Build.VERSION.SDK)
                < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    private static String getResponseText(InputStream inStream) {
        return new Scanner(inStream).useDelimiter("\\A").next();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (!mResponseMap.isEmpty()) {
            mResponseMap = new HashMap<>();
        }
    }

    public HashMap<String, String> requestWebService(String serviceUrl) {
        disableConnectionReuseIfNecessary();

        HttpURLConnection urlConnection = null;
        try {
            // create connection
            URL urlToRequest = new URL(serviceUrl);
            urlConnection = (HttpURLConnection)
                    urlToRequest.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setReadTimeout(30000);

            // handle issues
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
                mResponseMap.put(Constants.KEY_STATUS_MESSAGE, "Unauthorized Request.");
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
                mResponseMap.put(Constants.KEY_STATUS_MESSAGE, "Request Failed.");
            } else {
                // Request successful
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                JSONObject jsonObjectResponse = new JSONObject(getResponseText(in));
                mResponseMap.put(Constants.KEY_STATUS_MESSAGE, jsonObjectResponse.toString());
            }

            mResponseMap.put(Constants.KEY_STATUS_CODE, Integer.toString(statusCode));

        } catch (MalformedURLException e) {
            // URL is invalid
        } catch (SocketTimeoutException e) {
            // data retrieval or connection timed out
        } catch (IOException e) {
            // could not read response body
            // (could not create input stream)
        } catch (JSONException e) {
            // response body is no valid JSON string
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return mResponseMap;
    }

    public void setListener(NetworkListener listener) {
        mListener = listener;
    }

    @Override
    protected Boolean doInBackground(String... urls) {
        if (AppUtil.isConnectedToInternet(mContext)) {
            requestWebService(urls[0]);
            return true;
        } else {
            mResponseMap.put(Constants.KEY_STATUS_CODE, Integer.toString(503));
            mResponseMap.put(Constants.KEY_STATUS_MESSAGE, "You are not connected to the internet.");
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean responseStatus) {
        if (Integer.parseInt(mResponseMap.get(Constants.KEY_STATUS_CODE)) == 200) {
            mListener.onSuccess(mResponseMap.get(Constants.KEY_STATUS_MESSAGE));
        } else {
            mListener.onFailure(mResponseMap.get(Constants.KEY_STATUS_MESSAGE));
        }
    }

    public interface NetworkListener {
        void onSuccess(String response);

        void onFailure(String message);
    }
}
