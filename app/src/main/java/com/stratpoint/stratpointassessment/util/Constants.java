package com.stratpoint.stratpointassessment.util;

/**
 * Created by Carl Justine De leon on 10/23/2015.
 */
public class Constants {

    public static final String BASE_URL = "https://dl.dropboxusercontent.com/u/5624850/movielist/list_movies_page1.json";
    public static final String BASE_URL_COVER = "https://dl.dropboxusercontent.com/u/5624850/movielist/images/{slug}-cover.jpg";
    public static final String BASE_URL_BACKDROP = "https://dl.dropboxusercontent.com/u/5624850/movielist/images/{slug}-backdrop.jpg";

    public static final String KEY_STATUS_CODE = "status_code";
    public static final String KEY_STATUS_MESSAGE = "status_message";

    public static final String KEY_ARRAY_MOVIE = "movies";
}
