package com.stratpoint.stratpointassessment.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Carl Justine De leon on 10/23/2015.
 */
public class Movie implements Parcelable {

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {

        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    String mTitle, mTitleLong, mOverview, mSlug;
    int mId, mYear;
    float mRating;

    public Movie() {
    }

    public Movie(float rating, String title, String titleLong, String overview, String slug, int id, int year) {
        mRating = rating;
        mTitle = title;
        mTitleLong = titleLong;
        mOverview = overview;
        mSlug = slug;
        mId = id;
        mYear = year;
    }

    private Movie(Parcel in) {
        mRating = in.readFloat();
        mTitle = in.readString();
        mTitleLong = in.readString();
        mOverview = in.readString();
        mSlug = in.readString();
        mId = in.readInt();
        mYear = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeFloat(mRating);
        out.writeString(mTitle);
        out.writeString(mTitleLong);
        out.writeString(mOverview);
        out.writeString(mSlug);
        out.writeInt(mId);
        out.writeInt(mYear);
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        mRating = rating;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitleLong() {
        return mTitleLong;
    }

    public void setTitleLong(String titleLong) {
        mTitleLong = titleLong;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getSlug() {
        return mSlug;
    }

    public void setSlug(String slug) {
        mSlug = slug;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(int year) {
        mYear = year;
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
